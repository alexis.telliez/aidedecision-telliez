# aidedecision-telliez

- Telliez Alexis

## Résultats

### Résultats mono-objective
Les résultats sont testés avec les paramètres suivants:
- Temps d'une simulation 480 (-t=480)
- Minimisation Time ou CO2 (-F=1 ou -F=2)
- Population 100 (-P=100)

```
Time:
./src/cmaES -t=480 -F=1 -P=100
./src/DE -t=480 -F=1 -P=100

CO2:
./src/cmaES -t=480 -F=2 -P=100
./src/DE -t=480 -F=2 -P=100
```

#### Minimisation du temps

##### cmaES 

```
Fitness : 157083

Solution : 24 1.8 0.3 1.8 0.1 0.5 1.8 1 1.3 0.1 0.4 1.3 1.3 30 20 30 35 20 35 35 35 30 20 35 35 
```

##### DE 

```
Fitness : 156963

Solution : 24 1.55468 0.3 1.2 0.100147 0.879494 1.2 0.851598 0.7 0.102229 0.392131 0.7 1.1882 30 19.9196 30 35 20 35 35 25 29.9846 19.9542 33.5062 35
```
#### Minimisation du CO2

##### cmaES

```
Fitness : 587791

Solution : 24 1.2 0.3 1.2 0.1 1 1.2 0.5 0.7 0.1 0.4 0.7 0.7 20 10 20 25 10 25 25 25 20 10 30 30 
```

##### DE

```
Fitness : 541442

Solution : 24 1.23774 0.3 1.8 0.1 1 1.2 1 1.3 0.3 0.4 0.7 1.3 20 10 20 25 10.3379 25 25 25 20 10 30 30 
```
#### Explication des résultats

Sur les deux minimisations, on peut voir que le DE est plus efficace.




### Résultats multi-objective

Les résultats sont testés avec les paramètres suivants:
- Temps d'une simulation 480 (-t=480)
- Population 100 (-P=100)

```
./src/nsgaII -t=480 -P=100
```

