/**
 * @file LinkQueue.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2021 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TP4_LINK_QUEUE_HPP
#define TP4_LINK_QUEUE_HPP

#include <artis-star/kernel/pdevs/Dynamics.hpp>

#include "Base.hpp"
#include "Vehicle.hpp"

namespace tp4 {

    struct LinkQueueParameters {
        double length;
        unsigned int lane_number;
        double free_speed;
        double concentration;
        double wave_speed;
        double capacity;
        unsigned int out_link_number;
        int id_link;
    };

    class LinkQueue
        : public artis::pdevs::Dynamics<artis::common::DoubleTime, LinkQueue, LinkQueueParameters> {
    public:
        struct inputs {
            enum values {
                IN, IN_OPEN, IN_CLOSE, BACK
            };
        };

        struct outputs {
            enum values {
                OUT
            };
        };

        struct vars {
            enum values {
                VEHICLE_NUMBER, WAITING_DURATION_SUM
            };
        };

        LinkQueue(const std::string &name,
                  const artis::pdevs::Context<artis::common::DoubleTime, LinkQueue, LinkQueueParameters> &context)
            :
            artis::pdevs::Dynamics<artis::common::DoubleTime, LinkQueue, LinkQueueParameters>(name, context),
            _length(context.parameters().length),
            _lane_number(context.parameters().lane_number),
            _free_speed(context.parameters().free_speed),
            _concentration(context.parameters().concentration),
            _wave_speed(context.parameters().wave_speed),
            _capacity(context.parameters().capacity),
            _id_link(context.parameters().id_link),
            _open_links(context.parameters().out_link_number) {
          input_ports({{inputs::IN,       "in"},
                       {inputs::IN_OPEN,  "in_open"},
                       {inputs::IN_CLOSE, "in_close"},
                       {inputs::BACK,     "back"}});
          output_ports({{outputs::OUT, "out"}});
          observables({{vars::VEHICLE_NUMBER,       "vehicle_number"},
                       {vars::WAITING_DURATION_SUM, "waiting_duration_sum"}});

          for (unsigned int i = 0; i < context.parameters().out_link_number; ++i) {
            _fully_close_links.push_back(false);
          }
        }

        ~LinkQueue() override = default;

        void dconf(
            const Time & /* t* */,
            const Time & /* e */,
            const Bag & /* bag */) override;

        void dint(const Time & /* t */) override;

        void dext(
            const Time & /* t */,
            const Time & /* e */,
            const Bag & /* bag*/) override;

        void start(const Time & /* t */) override;

        Time ta(const Time & /* t */) const override;

        Bag lambda(const Time & /* t */) const override;

        artis::common::Value observe(const Time &t,
                                     unsigned int index) const override;

    private:
        void update_state(const Time &t);

        void update_sigma(const Time &t);

        bool vehicle_ready(const Time &t) const;

        struct Entry {
            Vehicle vehicle;
            Time in_time;
            Time next_time;
        };

        struct Phase {
            enum values {
                WAIT, SEND
            };

            static std::string to_string(const values &value) {
              switch (value) {
                case WAIT:
                  return "WAIT";
                case SEND:
                  return "SEND";
              }
              return "";
            }
        };

        // parameters
        double _length;
        unsigned int _lane_number;
        double _free_speed;
        double _concentration;
        double _wave_speed;
        double _capacity;
        int _id_link;

        // state
        std::deque<Entry> _vehicles;
        std::vector<double> _open_links;
        std::vector<bool> _fully_close_links;
        Time _sigma;
        Phase::values _phase;
        Time _last_time;
        double _waiting_duration_sum;
    };

}

#endif