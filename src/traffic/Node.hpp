/**
 * @file Node.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2021 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TP4_NODE_HPP
#define TP4_NODE_HPP

#include <artis-star/kernel/pdevs/Dynamics.hpp>
#include <random>

#include "Base.hpp"
#include "Vehicle.hpp"

namespace tp4 {

struct NodeParameters
{
  double transit_duration;
  unsigned int in_number;
  unsigned int out_number;
  std::vector<double> proportions;
};

class Node
    : public artis::pdevs::Dynamics<artis::common::DoubleTime, Node, NodeParameters>
{
public:
  struct inputs
  {
    enum values
    {
      IN, IN_OPEN = 500, IN_CLOSE = 1000
    };
  };

  struct outputs
  {
    enum values
    {
      OUT_OPEN, OUT_CLOSE, BACK, OUT = 1000, OUT_CONFIRM = 2000
    };
  };

  struct vars
  {
    enum values
    {
      ARRIVED, VEHICLE
    };
  };

  Node(const std::string &name,
       const artis::pdevs::Context<artis::common::DoubleTime, Node, NodeParameters> &context)
      :
      artis::pdevs::Dynamics<artis::common::DoubleTime, Node, NodeParameters>(
          name, context),
      _transit_duration(context.parameters().transit_duration),
      _in_number(context.parameters().in_number),
      _out_number(context.parameters().out_number),
      _proportions(context.parameters().proportions),
      _open_links(context.parameters().out_number, false)
  {

    assert(_proportions.size() == _in_number);

    for (unsigned int i = 0; i < _in_number; ++i) {
      _vehicle_numbers.push_back(0);
    }

    output_port({outputs::OUT_OPEN, "out_open"});
    output_port({outputs::OUT_CLOSE, "out_close"});
    for (unsigned int i = 0; i < _in_number; ++i) {
      std::stringstream ss_in, ss_back, ss_confirm;

      ss_in << "in_" << (i + 1);
      ss_back << "back_" << (i + 1);
      ss_confirm << "confirm_" << (i + 1);
      input_port({inputs::IN + i, ss_in.str()});
      output_port({outputs::BACK + i, ss_back.str()});
      output_port({outputs::OUT_CONFIRM + i, ss_confirm.str()});
    }
    for (unsigned int i = 0; i < _out_number; ++i) {
      std::stringstream ss_out, ss_in_open, ss_in_close;

      ss_out << "out_" << (i + 1);
      ss_in_open << "in_open_" << (i + 1);
      ss_in_close << "in_close_" << (i + 1);
      input_port({inputs::IN_OPEN + i, ss_in_open.str()});
      input_port({inputs::IN_CLOSE + i, ss_in_close.str()});
      output_port({outputs::OUT + i, ss_out.str()});
    }
    observables({{vars::ARRIVED, "arrived"},
                 {vars::VEHICLE, "vehicle"}});
  }

  ~Node() override = default;

  void dconf(const Time & /* t* */,
             const Time & /* e */,
             const Bag & /* bag */) override;

  void dint(const Time & /* t */) override;

  void dext(const Time & /* t */,
            const Time & /* e */,
            const Bag & /* bag*/) override;

  void start(const Time & /* t */) override;

  Time ta(const Time & /* t */) const override;

  Bag lambda(const Time & /* t */) const override;

  artis::common::Value observe(const Time &t,
                               unsigned int index) const override;

private:
  // types
  struct Phase
  {
    enum values
    {
      WAIT, TRANSIT, SEND, SEND_OPEN, SEND_CLOSE, SEND_BACK
    };

    static std::string to_string(const values &value)
    {
      switch (value) {
      case WAIT:return "WAIT";
      case TRANSIT:return "TRANSIT";
      case SEND:return "SEND";
      case SEND_OPEN:return "SEND_OPEN";
      case SEND_CLOSE:return "SEND_CLOSE";
      case SEND_BACK:return "SEND_BACK";
      }
      return "";
    }
  };

  // parameters
  double _transit_duration;
  unsigned int _in_number;
  unsigned int _out_number;
  std::vector<double> _proportions;

  // state
  Vehicle _vehicle;
  std::vector<std::pair<unsigned int, Vehicle>> _vehicles;
  bool _arrived;
  std::vector<double> _open_links;
  std::vector<size_t> _open_received;
  std::vector<std::pair<unsigned int, Vehicle>> _back_vehicles;
  std::vector<unsigned int> _vehicle_numbers;
  Time _sigma;
  Time _stored_sigma;
  Phase::values _phase;
  Phase::values _stored_phase;
  unsigned int _out_index;
  unsigned int _index;
  Time _last_time;
  bool _jam;
  unsigned int _vehicle_from;
};

}

#endif