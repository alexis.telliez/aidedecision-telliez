/**
 * @file Node.cpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2021 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include "Node.hpp"

namespace tp4 {

    void Node::dconf(const Time &t,
                     const Time &e,
                     const Bag &bag) {
      dint(t);
      dext(t, e, bag);
    }

    void Node::dint(const Time &t) {
      if (_phase == Phase::TRANSIT) {
        _phase = Phase::SEND;
        _sigma = 0;
      } else if (_phase == Phase::SEND) {
        _arrived = false;
        if (!_jam) {
          for (auto &n : _vehicle_numbers) {
            n = 0;
          }
        }
        _vehicles.clear();
        _phase = Phase::WAIT;
        _sigma = artis::common::DoubleTime::infinity;
      } else if (_phase == Phase::SEND_CLOSE) {
        if (_stored_phase == Phase::TRANSIT && _out_index == _index) {
          _phase = Phase::SEND_BACK;
          _sigma = 0;
          _back_vehicles.push_back({_vehicle_from, _vehicle});
          _vehicles.clear();
          _arrived = false;
        } else {
          _phase = _stored_phase;
          _sigma = _stored_sigma;
          _open_received.clear();
        }
      } else if (_phase == Phase::SEND_OPEN) {
        _phase = _stored_phase;
        _sigma = _stored_sigma;
        _open_received.clear();
      } else if (_phase == Phase::SEND_BACK) {
        _back_vehicles.clear();
        if (_arrived) {
          _phase = Phase::TRANSIT;
          _sigma = _transit_duration;
        } else {
          _phase = Phase::WAIT;
          _sigma = artis::common::DoubleTime::infinity;
        }
        _stored_phase = _phase;
        _stored_sigma = _sigma;
      }
      _last_time = t;
    }

    void Node::dext(const Time &t,
                    const Time &e,
                    const Bag &bag) {

      _stored_phase = _phase;
      if (_sigma > 0 and _sigma < artis::common::DoubleTime::infinity) {
        _stored_sigma = _sigma - e;
      } else {
        _stored_sigma = _sigma;
      }

      {

        std::for_each(bag.begin(), bag.end(),
                      [e, t, this](const ExternalEvent &event) {
                          if (event.port_index() >= inputs::IN
                              and event.port_index() < inputs::IN_OPEN) {
                            unsigned int index = event.port_index() - inputs::IN;
                            Vehicle vehicle;

                            event.data()(vehicle);
                            _vehicles.emplace_back(index, vehicle);
                          }
                      });
        if (not _vehicles.empty()) {
          if (_vehicles.size() == 1) {
            _vehicle = _vehicles.back().second;
            _vehicle_from = _vehicles.back().first;
            _vehicle.t_real_end = t;
            _phase = Phase::TRANSIT;
            _sigma = _transit_duration;
            _jam = false;
          } else if (_vehicles.size() > 1) {
            _jam = true;
            double min_distance = 0;
            size_t min_index = 0;
            size_t index = 0;
            double sum = std::accumulate(_vehicle_numbers.cbegin(), _vehicle_numbers.cend(), 0.f);
            double sumProp = std::accumulate(_proportions.cbegin(), _proportions.cend(), 0.f);

            while (index < _vehicle_numbers.size()) {
              double distance = _vehicle_numbers[index] / sum - _proportions[index] / sumProp;

              if (distance < 0 and min_distance > distance) {
                min_index = index;
                min_distance = distance;
              }
              ++index;
            }
            _vehicle = _vehicles.at(min_index).second;
            _vehicle.t_real_end = t;
            _vehicle_from = _vehicles.at(min_index).first;
            _vehicles.erase(_vehicles.cbegin() + min_index);
            _vehicle_numbers[min_index]++;
            _back_vehicles = _vehicles;

            _phase = Phase::SEND_BACK;
            _sigma = 0;
          }
          _arrived = true;
          _out_index =
              (_vehicle.path & (1 << _vehicle.path_index)) >> _vehicle.path_index;
          _stored_phase = _phase;
          _stored_sigma = _sigma;

        }
      }
      std::for_each(bag.begin(), bag.end(),
                    [e, t, this](const ExternalEvent &event) {
                        if (event.port_index() >= inputs::IN_OPEN
                            and event.port_index() < inputs::IN_CLOSE) {
                          _index = event.port_index() - inputs::IN_OPEN;

                          _open_links[_index] = artis::common::DoubleTime::infinity;
                          _open_received.push_back(_index);
                          _phase = Phase::SEND_OPEN;
                          _sigma = 0;
                        } else if (event.port_index() >= inputs::IN_CLOSE) {
                          double open_time;

                          event.data()(open_time);
                          _index = event.port_index() - inputs::IN_CLOSE;

                          _open_links[_index] = open_time;
                          _phase = Phase::SEND_CLOSE;
                          _sigma = 0;
                        }
                    }
      );
      _last_time = t;
    }

    void Node::start(const Time &t) {
      _arrived = false;
      _phase = Phase::WAIT;
      _sigma = artis::common::DoubleTime::infinity;
      _last_time = t;
      _jam = false;
    }

    Time Node::ta(const Time & /* t */) const {
      return _sigma;
    }

    Bag Node::lambda(const Time & /* t */) const {
      Bag bag;

      if (_phase == Phase::SEND) {
        if (_out_number == 1) {

          bag.push_back(artis::common::ExternalEvent<artis::common::DoubleTime>(outputs::OUT,
                                                                                _vehicle));
          bag.push_back(artis::common::ExternalEvent<artis::common::DoubleTime>(outputs::OUT_CONFIRM +
                                                                                _vehicle_from, _vehicle));
        } else {
          bag.push_back(artis::common::ExternalEvent<artis::common::DoubleTime>(outputs::OUT + _out_index,
                                                                                _vehicle));
          bag.push_back(artis::common::ExternalEvent<artis::common::DoubleTime>(outputs::OUT_CONFIRM +
                                                                                _vehicle_from, _vehicle));
        }
      } else if (_phase == Phase::SEND_CLOSE) {
        bag.push_back(artis::common::ExternalEvent<artis::common::DoubleTime>(outputs::OUT_CLOSE,
                                                                              Data{
                                                                                  (unsigned int) _index,
                                                                                  _open_links[_index]}));
      } else if (_phase == Phase::SEND_OPEN) {
        for (const auto &r:_open_received) {
          bag.push_back(artis::common::ExternalEvent<artis::common::DoubleTime>(outputs::OUT_OPEN,
                                                                                (unsigned int) r));
        }
      } else if (_phase == Phase::SEND_BACK) {
        for (const auto &v:_back_vehicles) {
          bag.push_back(artis::common::ExternalEvent<artis::common::DoubleTime>(outputs::BACK + v.first,
                                                                                v.second));
        }
      }
      return bag;
    }

    artis::common::Value Node::observe(const Time &t,
                                       unsigned int index) const {
      switch (index) {
        case vars::ARRIVED:
          return (unsigned int) (_arrived + _back_vehicles.size());
        case vars::VEHICLE:
          if (_vehicle.t_real_end == t - _transit_duration) {
            return artis::common::Value(_vehicle.getJson().c_str(), _vehicle.getJson().size());
          }
          return artis::common::Value();
        default:
          return artis::common::Value();
      }
    }

}