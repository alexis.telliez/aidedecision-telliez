/**
 * @file utils/Counter.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2021 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TP4_COUNTER_HPP
#define TP4_COUNTER_HPP

#include <artis-star/kernel/pdevs/Dynamics.hpp>
#include "Vehicle.hpp"

namespace tp4 {

    class Counter
        : public artis::pdevs::Dynamics<artis::common::DoubleTime, Counter> {
    public:
        struct inputs {
            enum values {
                IN
            };
        };

        struct outputs {
            enum values {
                OUT_CONFIRM
            };
        };

        struct vars {
            enum values {
                COUNTER, VEHICLE, TRAVEL_DURATION_SUM, TRAVEL_DURATION_MAX, TRAVEL_DURATION_MIN,
                CONSUMPTION_SUM, CONSUMPTION_MAX, CONSUMPTION_MIN
            };
        };

        Counter(const std::string &name,
                const artis::pdevs::Context<artis::common::DoubleTime, Counter> &context)
            :
            artis::pdevs::Dynamics<artis::common::DoubleTime, Counter>(name, context) {
          input_port({inputs::IN, "in"});
          observables({{vars::COUNTER,             "counter"},
                       {vars::VEHICLE,             "vehicle"},
                       {vars::TRAVEL_DURATION_SUM, "travel_duration_sum"},
                       {vars::TRAVEL_DURATION_MAX, "travel_duration_max"},
                       {vars::TRAVEL_DURATION_MIN, "travel_duration_min"},
                       {vars::CONSUMPTION_SUM,     "consumption_sum"},
                       {vars::CONSUMPTION_MAX,     "consumption_max"},
                       {vars::CONSUMPTION_MIN,     "consumption_min"}
                      });
          output_port({outputs::OUT_CONFIRM, "out_confirm"});
        }

        ~Counter() override = default;

        void dint(const Time & /* t */) override {
          if (_phase == Phase::SEND_CONFIRM) {
            _phase = Phase::WAIT;
            _sigma = artis::common::DoubleTime::infinity;
          }
        }

        void dext(const Time &t, const Time &e,
                  const Bag &bag) override {
          _counter += bag.size();

          std::for_each(bag.begin(), bag.end(),
                        [e, t, this](const ExternalEvent &event) {
                            Vehicle vehicle;

                            event.data()(vehicle);
                            _vehicle = vehicle;
                            _vehicle.t_real_end = t;
                            {
                              double duration = t - _vehicle.t_start;

                              _travel_duration_sum += duration;
                              if (_travel_duration_max < duration) {
                                _travel_duration_max = duration;
                              }
                              if (_travel_duration_min > duration) {
                                _travel_duration_min = duration;
                              }
                              _consumption_sum += _vehicle.consumption;
                              if (_consumption_max < vehicle.consumption) {
                                _consumption_max = vehicle.consumption;
                              }
                              if (_consumption_min > vehicle.consumption) {
                                _consumption_min = vehicle.consumption;
                              }
                            }
                            _phase = Phase::SEND_CONFIRM;
                            _sigma = 0;
                        });
        }

        void start(const Time & /* t */) override {
          _counter = 0;
          _travel_duration_sum = 0;
          _travel_duration_min = 1e9;
          _travel_duration_max = 0;
          _consumption_sum = 0;
          _consumption_min = 1e9;
          _consumption_max = 0;
          _sigma = artis::common::DoubleTime::infinity;
          _phase = Phase::WAIT;
          _vehicle = {};
          _vehicle.t_real_end = artis::common::DoubleTime::infinity;
        }

        Time ta(const Time & /* t */) const override {
          return _sigma;
        }

        Bag lambda(const Time & /* t */) const override {
          Bag bag;
          bag.push_back(artis::common::ExternalEvent<artis::common::DoubleTime>(outputs::OUT_CONFIRM, _vehicle));
          return bag;
        }

        artis::common::Value observe(const Time &t, unsigned int index) const override {
          if (index == vars::COUNTER) {
            return _counter;
          } else if (index == vars::VEHICLE) {
            if (_vehicle.t_real_end == t) {
              return artis::common::Value(_vehicle.getJson().c_str(), _vehicle.getJson().size());
            }
            return artis::common::Value();
          } else if (index == vars::TRAVEL_DURATION_SUM) {
            return (double) _travel_duration_sum;
          } else if (index == vars::TRAVEL_DURATION_MIN) {
            return (double) _travel_duration_min;
          } else if (index == vars::TRAVEL_DURATION_MAX) {
            return (double) _travel_duration_max;
          } else if (index == vars::CONSUMPTION_SUM) {
            return (double) _consumption_sum;
          } else if (index == vars::CONSUMPTION_MIN) {
            return (double) _consumption_min;
          } else if (index == vars::CONSUMPTION_MAX) {
            return (double) _consumption_max;
          } else {
            return artis::common::Value();
          }
        }

    private:
        struct Phase {
            enum values {
                WAIT, SEND_CONFIRM
            };

            static std::string to_string(const values &value) {
              switch (value) {
                case WAIT:
                  return "WAIT";
                case SEND_CONFIRM:
                  return "SEND_CONFIRM";
              }
              return "";
            }
        };

        unsigned int _counter;
        Time _sigma;
        Phase::values _phase;
        Vehicle _vehicle;
        double _travel_duration_sum;
        double _travel_duration_min;
        double _travel_duration_max;
        double _consumption_sum;
        double _consumption_min;
        double _consumption_max;
    };

}

#endif